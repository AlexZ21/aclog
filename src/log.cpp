#include <aclog/log.h>

namespace ac {

Log &Log::instance()
{
    static Log log;
    return log;
}

}
