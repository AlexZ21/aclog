#ifndef LOG_H
#define LOG_H

#include <iostream>
#include <sstream>
#include <chrono>
#include <ctime>
#include <iomanip>

#if defined(_MSC_VER)
#  define DECL_EXPORT     __declspec(dllexport)
#  define DECL_IMPORT     __declspec(dllimport)
#elif defined(__GNUC__)
#  if  defined(__WIN32__)
#    define DECL_EXPORT     __declspec(dllexport)
#    define DECL_IMPORT     __declspec(dllimport)
#  elif defined(__linux__)
#    define DECL_EXPORT     __attribute__((visibility("default")))
#    define DECL_IMPORT     __attribute__((visibility("default")))
#    define DECL_HIDDEN     __attribute__((visibility("hidden")))
#  endif
#endif

#if !defined(ACLOG_STATIC)
#  define ACLOG_EXPORT DECL_EXPORT
#else
#  define ACLOG_EXPORT DECL_IMPORT
#endif

#define LENABLE             ac::Log::enable();
#define LDISABLE            ac::Log::disable();
#define LFILTER(_filter_)   ac::Log::setFilter(_filter_);
#define LDEBUG(...)         ac::Log::debug(__VA_ARGS__);
#define LSDEBUG(...)        ac::Log::debug_s(__VA_ARGS__);
#define LINFO(...)          ac::Log::info(__VA_ARGS__);
#define LSINFO(...)         ac::Log::info_s(__VA_ARGS__);
#define LWARN(...)          ac::Log::warning(__VA_ARGS__);
#define LSWARN(...)         ac::Log::warning_s(__VA_ARGS__);
#define LERR(...)           ac::Log::error(__VA_ARGS__);
#define LSERR(...)          ac::Log::error_s(__VA_ARGS__);
#define LFATAL(...)         ac::Log::fatal(__VA_ARGS__);
#define LSFATAL(...)        ac::Log::fatal_s(__VA_ARGS__);

namespace ac {

class ACLOG_EXPORT Log
{
public:
    enum Level {
        L_DEBUG = 0x0001,
        L_INFO = 0x0002,
        L_WARNING = 0x0004,
        L_ERROR = 0x0008,
        L_FATAL = 0x0010
    };

    template <typename... Args>
    static void debug(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_DEBUG))
            return;
        log.push("DEBUG", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void debug_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_DEBUG))
            return;
        log.push("DEBUG", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void info(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_INFO))
            return;
        log.push("INFO", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void info_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_INFO))
            return;
        log.push("INFO", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void warning(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_WARNING))
            return;
        log.push("WARNING", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void warning_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_WARNING))
            return;
        log.push("WARNING", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void error(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_ERROR))
            return;
        log.push("ERROR", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void error_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_ERROR))
            return;
        log.push("ERROR", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void fatal(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_FATAL))
            return;
        log.push("FATAL", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void fatal_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & L_FATAL))
            return;
        log.push("FATAL", true, std::forward<Args>(args)...);
    }

    static void enable() {
        instance().m_enable = true;
    }
    static void disable() {
        instance().m_enable = false;
    }
    static void setFilter(int filter) {
        instance().m_filter = filter;
    }

private:
    static Log &instance();

    Log() : m_enable(false), m_filter(L_DEBUG | L_INFO | L_WARNING | L_ERROR | L_FATAL) {}

    template <typename... Args>
    void push(const std::string &level, bool spacing, Args &&...args) {
        std::string s = spacing ? " " : "";
        m_stream << '[' << currentTime() << ']';
        m_stream << '[' << level << ']' << " ";
        using expander = int[];
        (void)expander{0, (void(m_stream << std::forward<Args>(args) << s), 0)...};
        print();
    }

    template <typename Arg>
    void push(std::stringstream &stream, Arg &&arg) {
        stream << std::forward<Arg>(arg);
    }

    std::string currentTime() const {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        std::time_t now_c = std::chrono::system_clock::to_time_t(now - std::chrono::hours(24));
        std::string currentTime = std::ctime(&now_c);
        currentTime.pop_back();
        return currentTime;
    }

    void print() {
        std::cout << m_stream.str() << std::endl;
        m_stream.str(std::string());
        m_stream.clear();
    }

private:
    std::stringstream m_stream;
    bool m_enable;
    int m_filter;

};

}

#endif // LOG_H
