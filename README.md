# README #

Simple class for logging.

### Usage ###
```C++
#include <aclog/log.h>

int main(int argc, char *argv[])
{
	LENABLE;
	LSINFO("Hello world!", 3.14, 10, "Some text");
	return 0;
}
```

Output:
```
[Wed Jan 24 12:54:18 2018][INFO] Hello world! 3.14 10 Some text
```

